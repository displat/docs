# Paradox User Guide

Hi, glad to have you! Welcome to Paradox, a PaaS oriented around Discord Bots. Paradox aims to eliminate as much of the architectural and infrastructural challenges that come with hosting Discord bots at scale. The hope is that Paradox will allow developers to forget about infrastructure while staying cost-effective, all from day 1.

This guide will cover everything you need to know, whether using our managed instance or self-hosting.
